<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends BaseModel
{
    public $table = 'contact_us';
    public $fillable = ['firstName','lastName','email','message'];
}
