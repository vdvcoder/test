<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\ContactUS;
use App\Mail\SendMail;

class ContactUsController extends Controller
{
    /**
    * Save the contact data.
    *
    * @return \Illuminate\Http\Response
    */
   public function contactSaveData(Request $request)
   {
       //Validate request data
       $this->validate($request, [
        'firstName' => 'required|max:30',
        'lastName' => 'required|max:50',
        'email' => 'required|email',
        'message' => 'required|min:5|max:1000'
        ]);

       //Save contact data in database.
       ContactUs::create($request->all());

       //Create a data array with the email information.
       $data = array(
        'firstName' => $request->firstName,
        'lastName' => $request->firstName,
        'email' => $request->email,
        'message' => $request->message
        );

        //Send Mail to info@client.be
        Mail::to('info@client.be')->send(new SendMail($data));

        //Redirect terug naar het contact formulier.
        return back()->with('success', 'Thanks for contacting us!');
   }
}
