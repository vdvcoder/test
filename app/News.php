<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends BaseModel
{
    //Set mass-assignable fields
    protected $fillable = ['title', 'author', 'intro', 'content'];

    //A post can have only have 1 author
    public function author()
    {
        return $this->belongsTo('User','author');
    }
}
