<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/news', function () {
    return view('news');
})->name('news');

Route::get('/news/detail', function(){
    return view('news_detail');
})->name('new.details');

Route::get('/contact', function () {
    return view('contact');
})->name('contact');

Route::post('/contact', 'ContactUsController@contactSaveData')->name('contact.savedata');

Route::resource('/cms/news', 'NewsController'); //Creates all resources needed for crud

/* php artisan route:list
+--------+-----------+----------------------+--------------+---------------------------------------------+--------------+
| Domain | Method    | URI                  | Name         | Action                                      | Middleware   |
+--------+-----------+----------------------+--------------+---------------------------------------------+--------------+
|        | GET|HEAD  | /                    | home         | Closure                                     | web          |
|        | GET|HEAD  | api/user             |              | Closure                                     | api,auth:api |
|        | GET|HEAD  | cms/news             | news.index   | App\Http\Controllers\NewsController@index   | web          |
|        | POST      | cms/news             | news.store   | App\Http\Controllers\NewsController@store   | web          |
|        | GET|HEAD  | cms/news/create      | news.create  | App\Http\Controllers\NewsController@create  | web          |
|        | GET|HEAD  | cms/news/{news}      | news.show    | App\Http\Controllers\NewsController@show    | web          |
|        | PUT|PATCH | cms/news/{news}      | news.update  | App\Http\Controllers\NewsController@update  | web          |
|        | DELETE    | cms/news/{news}      | news.destroy | App\Http\Controllers\NewsController@destroy | web          |
|        | GET|HEAD  | cms/news/{news}/edit | news.edit    | App\Http\Controllers\NewsController@edit    | web          |
|        | GET|HEAD  | contact              | contact      | Closure                                     | web          |
|        | GET|HEAD  | news                 | news         | Closure                                     | web          |
|        | GET|HEAD  | news/detail          | new.details  | Closure                                     | web          |
+--------+-----------+----------------------+--------------+---------------------------------------------+--------------+ */


// Route::get('/cms/news', function() {
//     return view('cms.news.index');
// })->name('news.index');

// Route::get('/cms/news/create', function() {
//     return view('cms.news.edit');
// })->name('news.create');


