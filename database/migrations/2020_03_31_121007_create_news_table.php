<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->string('title', 100)->unique();  //Title: string, max-lengt of 100 chars.
            $table->string('author', 30); // Author: string, max-length of 30 chars.
            $table->string('intro', 1000); // Intro: string, max-length of 1000 chars.
            $table->string('content', 2000); // Content: string, max-length of 2000 chars.
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
