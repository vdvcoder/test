<x-layout>
    <div class="container mt-5">
        <div class="col-md-12">
            <a class="card mb-4 shadow-sm text-decoration-none" href="#">
                <div class="card-body text-dark">
                    <h3>{{ $news->title }}</h3>
                    <p class="card-text">{{ $news->content }}</p>
                    <div class="text-right">
                        <small class="text-muted">{{ $news->author}}</small>
                    </div>
                </div>
            </a>
        </div>
    </div>
</x-layout>
