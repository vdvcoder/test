<x-layout>
    <div class="container mt-5">
        <div class="row">
            @foreach(App\News::get() as $post)

                <div class="col-md-4">
                    <a class="card mb-4 shadow-sm text-decoration-none" href="{{ route('news.show',$post->id) }}">
                        <svg class="bd-placeholder-img card-img-top" width="100%" height="225"
                             xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false"
                             role="img" aria-label="Placeholder: Thumbnail"><title>Placeholder</title>
                            <rect width="100%" height="100%" fill="#55595c"></rect>
                            <text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text>
                        </svg>
                        <div class="card-body text-dark">
                            <h3>{{$post->title}}</h3>
                            <p class="card-text">{{$post->content}}</p>
                            <div class="text-right">
                                <small class="text-muted">{{$post->author}}</small>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</x-layout>
