<x-cms>
    <div class="container">

         <!-- Show success message if News post is created successfully -->
         @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <p>{{ $message }}</p>
            </div>
         @endif

         <!-- Form with POST method, action to route 'news.store' -->
        <form method="POST" action="{{ route('news.store')}}">
            {{ csrf_field() }} <!-- CSRF TO PREVENT CROSS SITE REQUEST FORGERY -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Editing news item</h6>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" name="title">
                        </div>
                        <div class="form-group">
                            <label for="author">Author</label>
                            <select id="author" name="author" class="form-control">
                            @foreach (App\User::get() as $author)
                                <option name='{{ $author->name }}' value='{{ $author->name }}'>{{ $author->name }}</option>
                            @endforeach
                        </select>
                        </div>
                        <div class="form-group">
                            <label for="intro">Intro</label>
                            <textarea name="intro" cols="30" rows="3" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="content">HTML</label>
                            <textarea name="content" cols="30" rows="10" class="form-control"></textarea>
                        </div>
                        <button class="btn btn-primary">Save</button>
                    </div>
                </div>
        </form>
    </div>
</x-cms>
