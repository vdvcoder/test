<x-cms>
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">News</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Intro</th>
                            <th>Author</th>
                            <th>Created At</th>
                        </tr>
                        </thead>
                            <tbody>
                                @foreach (App\News::get() as $post)
                                    <tr>
                                        <td>{{ $post->title}}</td>
                                        <td>{{ $post->intro}}</td>
                                        <td>{{ $post->author}}</td>
                                        <td>{{ $post->created_at}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-cms>
